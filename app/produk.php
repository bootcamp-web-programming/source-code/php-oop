<?php

namespace app\oop;

class Produk
{
    private $namaProduk;
    private $harga;

    public function setNamaProduk($val)
    {
        $this->namaProduk = $val;
    }

    public function getNamaProduk()
    {
        return $this->namaProduk;
    }

    public function setHarga($val)
    {
        $this->harga = $val;
    }

    public function getHarga()
    {
        return $this->harga;
    }
}

<?php

namespace app\oop;

class Kategori
{
    private $jenisKategori;

    public function setJenisKategori($val)
    {
        $this->jenisKategori = $val;
    }

    public function getJenisKategori()
    {
        return $this->jenisKategori;
    }
}

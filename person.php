<?php
class Person
{
    public $nama;
    public $alamat;
    public $wargaNegara = "Indonesia";
    const LAKI_LAKI = "Laki-Laki";
    const PEREMPUAN = "Perempuan";

    public function greeting()
    {
        return "Selamat Datang di Bootcamp Web Programming";
    }

    public function perkenalan($umur)
    {
        return "Perkenalkan, Nama saya " . $this->nama . ". Umur " . $umur;
    }

    public function panggilan($nama = NULL)
    {
        if (is_null($nama)) {
            return "Halo, Nama saya " . $this->nama;
        } else {
            return "Halo, Nama saya " . $this->nama . " panggil saja $nama";
        }
    }
}

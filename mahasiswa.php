<?php
require('person.php');

class Mahasiswa extends Person
{
    public $nim;

    public function halo()
    {
        return "NIM saya " . $this->nim;
    }

    public function perkenalan($umur)
    {
        return "Perkenalkan, Nama saya " . $this->nama .
            " NIM " . $this->nim .
            ". Umur " . $umur;
    }
}

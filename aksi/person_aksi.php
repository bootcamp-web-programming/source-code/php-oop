<?php
require('../person.php');

$andi = new Person();
$andi->nama = "Andi Dharmawan";
$andi->alamat = "Pontianak";

$santi = new Person();
$santi->nama = "Santi Eka Pratiwi";
$santi->alamat = "Kubu Raya";
$santi->wargaNegara = "Malaysia";

echo "Nama : " . $andi->nama . "<br>";
echo "Alamat : " . $andi->alamat . "<br>";
echo "Kewarganegaraan : " . $andi->wargaNegara . "<br>";
echo "Jenis Kelamin : " . Person::LAKI_LAKI . "<br>";
echo $andi->greeting() . "<br>";
echo $andi->perkenalan(23) . "<br>";
echo $andi->panggilan() . "<br>";
echo "===================================<br>";
echo "Nama : {$santi->nama}<br>";
echo "Alamat : {$santi->alamat}<br>";
echo "Kewarganegaraan : {$santi->wargaNegara}<br>";
echo "Jenis Kelamin : " . Person::PEREMPUAN . "<br>";
echo $santi->greeting() . "<br>";
echo $santi->perkenalan(20) . "<br>";
echo $santi->panggilan("Santi") . "<br>";

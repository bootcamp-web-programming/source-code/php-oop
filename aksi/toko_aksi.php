<?php
require_once('../app/kategori.php');
require_once('../app/produk.php');

use app\oop\Kategori;

$elektronik = new Kategori();
$elektronik->setJenisKategori("Elektronik");

$sharp = new app\oop\Produk();
$sharp->setNamaProduk("Sharp Aquos 60 Inch");
$sharp->setHarga(6000000);

echo "Nama Produk : " . $sharp->getNamaProduk() . "<br>";
echo "Kategori : " . $elektronik->getJenisKategori() . "<br>";
echo "Harga : " . $sharp->getHarga() . "<br>";

<?php
class Elektronik
{
    protected $merk;

    public function setMerk($merk)
    {
        $this->merk = $merk;
    }
}

class Televisi extends Elektronik
{
    public $nama;
    private $inci;

    public function spesifikasi()
    {
        return "Merk : " . $this->merk . " " . $this->nama . "<br>Inci : " . $this->inci;
    }

    public function setInci($inci)
    {
        $this->inci = $inci;
    }
}

<?php
class MataKuliah
{
    private $kode;
    private $nama;

    public function setKode($val)
    {
        $this->kode = $val;
    }

    public function getKode()
    {
        return $this->kode;
    }

    public function setNama($val)
    {
        $this->nama = $val;
    }

    public function getNama()
    {
        return $this->nama;
    }
}
